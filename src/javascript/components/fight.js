import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    document.addEventListener('keydown', function(event) {
      if(event.code == controls.PlayerOneAttack && event.code != controls.PlayerOneBlock){
        if(event.code != controls.PlayerTwoBlock) {
          getDamage(firstFighter, secondFighter);
          console.log('first attack');
        }
      }
        
      if(event.code == controls.PlayerTwoAttack && event.code != controls.PlayerTwoBlock) {
        if(event.code != controls.PlayerOneBlock) {
          getDamage(secondFighter, firstFighter);
          console.log('Second attack');
        }
      }
    })
  });
}

export function getDamage(attacker, defender) {
  // return damage
  let damage = getHitPower(attacker) - getBlockPower(defender);
  return damage < 0? 0 : damage;
}

export function getHitPower(fighter) {
  // return hit power
  return fighter['attack'] * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  // return block power
  return fighter['defense'] * (Math.random() + 1);
}
